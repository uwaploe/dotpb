package dotpb

import (
	"container/list"
	"errors"
	"fmt"
	io "io"
	"sort"
	"strconv"
	"strings"
	"time"

	"google.golang.org/protobuf/reflect/protoreflect"
)

//go:generate ./gen.sh

const (
	AngleScale    float32 = 10
	PressureScale float32 = 1000
	AccelScale    float32 = 1000
	GyroScale     float32 = 1000
)

const CsvTimeFormat = "2006-01-02 15:04:05.999"

var ErrRead = errors.New("Short read")

type Sample interface {
	// Pressure returns the water pressure in psi
	Pressure() float32
	// Heading returns the magnetic heading in degrees
	Heading() float32
	// Pitch returns the pitch angle in degrees
	Pitch() float32
	// Roll returns the roll angle in degrees
	Roll() float32
	// Accel returns the acceleration values in Gs
	Accel() [3]float32
	// Gyro returns the rotation rates in rad/s
	Gyro() [3]float32
	// SnowLevel returns the snow level in mm
	SnowLevel() uint32
}

// Create a new Record from a data sample.
func NewRecord(t time.Time, s Sample) *Record {
	rec := &Record{
		Time:     t.UnixNano(),
		Pressure: int32(s.Pressure() * PressureScale),
		Heading:  uint32(s.Heading() * AngleScale),
		Pitch:    int32(s.Pitch() * AngleScale),
		Roll:     int32(s.Roll() * AngleScale),
		Slevel:   s.SnowLevel(),
	}

	accel := s.Accel()
	gyro := s.Gyro()

	rec.Accel = &Ivec{
		X: int32(accel[0] * AccelScale),
		Y: int32(accel[1] * AccelScale),
		Z: int32(accel[2] * AccelScale),
	}

	rec.Gyro = &Ivec{
		X: int32(gyro[0] * GyroScale),
		Y: int32(gyro[1] * GyroScale),
		Z: int32(gyro[2] * GyroScale),
	}

	return rec
}

// ReadRecord returns the next record frame from rdr
func ReadRecord(rdr io.Reader) (*Record, error) {
	char := make([]byte, 1)
	_, err := rdr.Read(char)
	if err != nil {
		return nil, err
	}

	size := int(char[0])
	b := make([]byte, size)
	n, err := rdr.Read(b)
	if err != nil {
		return nil, err
	}

	if n != size {
		return nil, ErrRead
	}

	r := &Record{}
	err = r.UnmarshalVT(b)

	return r, err
}

// AsFrame returns the contents of the Record as a length-prefixed binary
// string. The length is stored as a single byte.
func (r *Record) AsFrame() ([]byte, error) {
	n := r.SizeVT()
	b := make([]byte, n+1)
	b[0] = byte(n)
	_, err := r.MarshalToVT(b[1:])
	return b, err
}

// AsCsv returns the contents of the Record in CSV format
func (r *Record) AsCsv() string {
	t := time.Unix(r.Time/1e9, r.Time%1e9)
	return fmt.Sprintf("%s,%.3f,%.1f,%.1f,%.1f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d",
		t.UTC().Format(CsvTimeFormat),
		float32(r.Pressure)/PressureScale,
		float32(r.Heading)/AngleScale,
		float32(r.Pitch)/AngleScale,
		float32(r.Roll)/AngleScale,
		float32(r.Accel.X)/AccelScale,
		float32(r.Accel.Y)/AccelScale,
		float32(r.Accel.Z)/AccelScale,
		float32(r.Gyro.X)/GyroScale,
		float32(r.Gyro.Y)/GyroScale,
		float32(r.Gyro.Z)/GyroScale,
		r.Slevel)
}

// Return a list of the Record field names.
func (r *Record) FieldNames() []string {
	return []string{
		"time",
		"pressure",
		"heading",
		"pitch",
		"roll",
		"accel.x",
		"accel.y",
		"accel.z",
		"gyro.x",
		"gyro.y",
		"gyro.z",
		"snow-level",
	}
}

// Store the index and JSON name of each field
type fieldInfo struct {
	Index, Name string
}

// Collect field info while traversing the message structure
type walker struct {
	nameStack  *list.List
	indexStack *list.List
	fields     []fieldInfo
}

func listToString(l *list.List) string {
	s := make([]string, 0, l.Len())
	for e := l.Front(); e != nil; e = e.Next() {
		s = append(s, e.Value.(string))
	}

	return strings.Join(s, ".")
}

// Return a function to be called by Message.Range to collect names and indicies
func (w *walker) walk() func(protoreflect.FieldDescriptor, protoreflect.Value) bool {
	return func(fd protoreflect.FieldDescriptor, v protoreflect.Value) bool {
		e_name := w.nameStack.PushBack(fd.JSONName())
		e_idx := w.indexStack.PushBack(strconv.Itoa(int(fd.Number())))
		if fd.Kind() == protoreflect.MessageKind {
			child := v.Message()
			child.Range(w.walk())
		} else {
			w.fields = append(w.fields,
				fieldInfo{Index: listToString(w.indexStack),
					Name: listToString(w.nameStack)})
		}
		w.nameStack.Remove(e_name)
		w.indexStack.Remove(e_idx)
		return true
	}
}

// Part of sort.Interface
func (w *walker) Less(i, j int) bool {
	return w.fields[i].Index < w.fields[j].Index
}

// Part of sort.Interface
func (w *walker) Len() int {
	return len(w.fields)
}

// Part of sort.Interface
func (w *walker) Swap(i, j int) {
	w.fields[i], w.fields[j] = w.fields[j], w.fields[i]
}

// Dynamically determine the field names for CSV output (WIP)
func (r *Record) xFieldNames() []string {
	w := &walker{
		nameStack:  list.New(),
		indexStack: list.New(),
		fields:     make([]fieldInfo, 0),
	}
	m := r.ProtoReflect()
	m.Range(w.walk())

	// Range visits each populated field in an undefined order so
	// we need to sort the names in index order
	sort.Sort(w)
	fields := make([]string, 0, w.Len())
	for _, f := range w.fields {
		fields = append(fields, f.Name)
	}

	return fields
}
