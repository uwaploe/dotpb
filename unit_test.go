package dotpb

import (
	"bytes"
	reflect "reflect"
	"testing"
	"time"

	"google.golang.org/protobuf/proto"
)

type sensor struct{}

func (s *sensor) Pressure() float32 {
	return 1
}

func (s *sensor) Heading() float32 {
	return 42
}

func (s *sensor) Pitch() float32 {
	return 3.14
}

func (s *sensor) Roll() float32 {
	return -1.414
}

func (s *sensor) Accel() [3]float32 {
	return [3]float32{0.1, 0.1, 1.0}
}

func (s *sensor) Gyro() [3]float32 {
	return [3]float32{0.1, 0.1, 0.1}
}

func (s *sensor) SnowLevel() uint32 {
	return 4242
}

func TestCsvNames(t *testing.T) {
	expected := []string{
		"time",
		"pressure",
		"heading",
		"pitch",
		"roll",
		"accel.x",
		"accel.y",
		"accel.z",
		"gyro.x",
		"gyro.y",
		"gyro.z",
		"snow-level",
	}
	rec := NewRecord(time.Now(), &sensor{})
	names := rec.FieldNames()

	if !reflect.DeepEqual(names, expected) {
		t.Errorf("Bad list; expected %v, got %v", expected, names)
	}
}

func TestFrame(t *testing.T) {
	rec := NewRecord(time.Now(), &sensor{})

	frame, err := rec.AsFrame()
	if err != nil {
		t.Fatal(err)
	}

	n := proto.Size(rec)
	if int(frame[0]) != n {
		t.Errorf("Wrong size; expectd %d, got %d", n, frame[0])
	}

	dup := &Record{}
	err = proto.Unmarshal(frame[1:], dup)
	if !proto.Equal(dup, rec) {
		t.Errorf("Record mismatch; expected %q, got %q", rec, dup)
	}
}

func TestReadRecord(t *testing.T) {
	rec := NewRecord(time.Now(), &sensor{})

	frame, err := rec.AsFrame()
	if err != nil {
		t.Fatal(err)
	}

	var b bytes.Buffer
	b.Write(frame)

	dup, err := ReadRecord(&b)
	if err != nil {
		t.Fatal(err)
	}

	if !proto.Equal(dup, rec) {
		t.Errorf("Record mismatch; expected %q, got %q", rec, dup)
	}
}
