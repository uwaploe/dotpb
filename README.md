# DOT Attitude and Pressure Data Format

This repository contains the [Protocol Buffer](https://developers.google.com/protocol-buffers) definitions for the storage of the DOT Buoy pressure and attitude sensor data. It also provides a Go package to read and write the data.
